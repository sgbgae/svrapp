<?php
require_once 'google/appengine/api/mail/Message.php';
use google\appengine\api\mail\Message;
syslog(LOG_INFO, "MULAI approval MKT");
	include_once '_conn/query.php';
	include_once 'functionread.php';
	$table_terimaemai = new query('SIMIS','SIMIS.TERIMAEMAIL');
	try{
		$table_terimaemai->update2("(SUBYEK LIKE 'APPROVAL/MKT/PAKETAN%' OR SUBYEK LIKE 'APPROVAL/MKT/BATASAN%') AND PROSES = 'F'", array(
			'PROSES' => 'T'
	));	
	}catch(Exception $e){
		//echo '<br/>Error: '.$e->getMessage();
		syslog(LOG_INFO, 'Error: '.$e->getMessage());
	}
	
	$database = 'MARKETING';
	$table_listapp = new query('MODUL', 'MODUL.LISTAPP L, SIMIS.TERIMAEMAIL T');
	syslog(LOG_INFO, "QUERY MODUL 669 " );
	$listapp = $table_listapp->selectBy('DISTINCT L.SUBYEK_APP, TABEL, L.STATUS AS TABEL_STATUS, L.ISISTATUS, L.TRANSAKSIID AS TABEL_TRANSAKSI, L.FUNCTION'," L.AKTIF = 'Y' AND L.STATUS <> '' AND ISISTATUS <> '' AND L.TRANSAKSIID <> ''
	AND T.SUBYEK LIKE CONCAT('%', L.SUBYEK_APP, '%') 
	AND T.PROSES = 'F'");
	syslog(LOG_INFO, " TABLE LISTAPP 673");
	//$currentlistapp = $listapp->current;
	//echo $listapp->printquery();
	//exit;
	foreach($listapp as $listapp){
	$tabel = $listapp->TABEL;
	$subyekapp = $listapp->SUBYEK_APP;
	$tabelstatus = $listapp->TABEL_STATUS;
	$isistatus = $listapp->ISISTATUS;
	$tabeltransaksi = $listapp->TABEL_TRANSAKSI;
	$function = $listapp->FUNCTION;
	$table_approval = new query('SIMIS', ''.$database.'.REQUESTCODE R, SIMIS.TERIMAEMAIL T, '.$tabel.' M, SIMIS.MASTER_USER_ACCOUNT U');
	$approval = $table_approval->selectBy("distinct T.ISIEMAIL AS ISIEMAIL, R.REQUESTCODE_ID, R.USERNAME, R.MASTERTRANSAKSI_ID, R.NOTICKET, R.URUTAN, case when (substring(substring(Trim(substring(T.ISIEMAIL,locate('>',T.ISIEMAIL)+1)),1,100),locate('Y:',substring(Trim(substring(T.ISIEMAIL,locate('>',T.ISIEMAIL)+1)),1,100))+2,6) = R.ACCEPTCODE)
		then 'Y'
		when (substring(substring(Trim(substring(T.ISIEMAIL,locate('>',T.ISIEMAIL)+1)),1,100),locate('T:',substring(Trim(substring(T.ISIEMAIL,locate('>',T.ISIEMAIL)+1)),1,100))+2,6) = R.REJECTCODE)
		then 'T' 
		END AS SETUJU, U.NIK, U.NAMA ","U.ALAMATEMAIL = R.USERNAME AND T.PROSES = 'F'
		AND LTRIM(RTRIM(substring(T.SUBYEK,locate('".$subyekapp."',T.SUBYEK)+length('".$subyekapp."')))) LIKE CONCAT(R.NOTICKET,'%')
		AND T.SUBYEK LIKE CONCAT('%','".$subyekapp."', '%')
		AND R.USERNAME = SUBSTRING( T.ISIEMAIL, locate('<',T.ISIEMAIL)+1,locate('>',T.ISIEMAIL)-locate('<',T.ISIEMAIL)-1)
		And ((substring(substring(Trim(substring(T.ISIEMAIL,locate('>',T.ISIEMAIL)+1)),1,100),locate('Y',substring(Trim(substring(T.ISIEMAIL,locate('>',T.ISIEMAIL)+1)),1,100))+2,6) = R.ACCEPTCODE 
		AND R.SETUJU IS NULL
		AND TGL_CONFIRM IS NULL) OR (substring(substring(Trim(substring(T.ISIEMAIL,locate('>',T.ISIEMAIL)+1)),1,100),locate('T',substring(Trim(substring(T.ISIEMAIL,locate('>',T.ISIEMAIL)+1)),1,100))+2,6) = R.REJECTCODE ))
		AND M.".$tabelstatus." = '".$isistatus."'
		AND M.".$tabeltransaksi." = R.MASTERTRANSAKSI_ID ");
	//$currentapproval = $approval->current;
	//echo $approval->printquery();	
	//echo "APPROVAL ".$approval->num_rows()." Record";
	
	syslog(LOG_INFO, "APPROVAL ".$approval->num_rows()." Record");
	//exit;
		foreach($approval as $approval){
			$mastertransaksiid = $approval->MASTERTRANSAKSI_ID;
			$setuju = $approval->SETUJU;
			$noticket = $approval->NOTICKET;
			$urutan = $approval->URUTAN;
			$nextU = $urutan+1;
			$username = $approval->USERNAME;
			$requestcodeid = $approval->REQUESTCODE_ID;
			$namavalidator = $approval->NAMA;
			$nikvalidator = $approval->NIK;
			$isiemail = $approval->ISIEMAIL; //echo $isiemail;
			$pos1 = strpos($isiemail,"#")+1;
			$isiemailreply2 = substr($isiemail,$pos1,strlen($isiemail)); 
			$pos2 = $pos1+strpos($isiemailreply2,'#');
			$konfirmasi = ' ';
			if ( $pos1 < 100 ) {
				if ( $pos2 > $pos1+2 )  $konfirmasi = substr($isiemail,$pos1,$pos2-$pos1);
			}
			if ( $pos1 > 100 ) $konfirmasi = ' ';
			
			$table_requestcode = new query($database,''.$database.'.REQUESTCODE');
			try{
				$table_requestcode->update2("REQUESTCODE_ID = '".$requestcodeid."'", array(
								'SETUJU' => $setuju,
								'KONFIRMASI' => $konfirmasi,
								'TGL_CONFIRM' => date('Y-m-d H:i:s')
			));	
			}catch(Exception $e){
				//echo '<br/>Error: '.$e->getMessage();
				syslog(LOG_INFO, 'Error: '.$e->getMessage());
			}
				
			if($setuju == 'Y'){
				
				$ceknextvalidator = $table_requestcode->selectBy("case when(URUTAN <> '') 
					then 'urut'
					when(URUTAN = '' OR URUTAN IS NULL)
					then 'tdkurut'
				END AS TIPE, ISIEMAIL,USERNAME, SUBJECT"," MASTERTRANSAKSI_ID = '".$mastertransaksiid."' AND NOTICKET = '".$noticket."' AND SETUJU IS NULL AND TGL_CONFIRM IS NULL AND ((URUTAN = '' OR URUTAN IS NULL) OR URUTAN = '".$nextU."')");	
				//$currentapproval = $ceknextvalidator->current;
				if($ceknextvalidator->num_rows() <> 0){
					foreach($ceknextvalidator as $ceknextvalidator){
						if($ceknextvalidator->TIPE == 'urut'){
							$isiemail1 = $ceknextvalidator->ISIEMAIL;
							$sendto = $ceknextvalidator->USERNAME;
							$mailsubject = $ceknextvalidator->SUBJECT;
							$isiemailapp = 'Permintaan ini disetujui oleh '.$namavalidator.' ('.$nikvalidator.')

'.$isiemail1; 
							//ECHO $mailsubject." ".$sendto." ".$isiemailapp;
							syslog(LOG_INFO, $mailsubject." ".$sendto." ".$isiemailapp);
							$mail_options = [   
							 "sender" => "serversgb@saligadingbersama.com", 
							 "to" => $sendto,   
							 "bcc" => "serversgb@saligadingbersama.com",   
							 "subject" => $mailsubject, 
							 "textBody" => $isiemailapp
							];	 
										
							IF ($sendto !== '' ) {
								try {    
									$emaildikirim = new Message($mail_options);    
									$emaildikirim->send();
									//echo "Selesai mengirim ke ".$sendto; 
									syslog(LOG_INFO, "Selesai mengirim ke ".$sendto ); 		
								} 
								catch (InvalidArgumentException $e) {
									//echo $e;	
									syslog(LOG_INFO, $e);
								}
							}
						} else if($ceknextvalidator->TIPE == 'tdkurut'){
							// echo "break";
							syslog(LOG_INFO, "break validator tidak urut");
						}
						break;
					}
				} else { 
					$ya = new marketing($setuju,$mastertransaksiid);  //('NAMA DATABASE','NAMA TABEL')
					$ya->$function();
				}
			} else if($setuju == 'T'){
				$tolak = new marketing($setuju,$mastertransaksiid);  //('NAMA DATABASE','NAMA TABEL')
				$tolak->$function(); //generate id
			}
			
			$table_terimaemail= new query('SIMIS','SIMIS.TERIMAEMAIL');
			try{
				$table_terimaemail->update2("ISIEMAIL = '".$isiemail."'", array(
								'PROSES' => 'T'
			));	
			}catch(Exception $e){
				//echo '<br/>Error: '.$e->getMessage();
				syslog(LOG_INFO, $e->getMessage());
			}		
			//ECHO $setuju;
			syslog(LOG_INFO, 'Selesai MKT : setuju');
			//exit;	
		}

	}
	syslog(LOG_INFO, "SELESAI approval MKT");

?>